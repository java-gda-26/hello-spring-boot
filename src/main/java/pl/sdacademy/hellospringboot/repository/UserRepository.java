package pl.sdacademy.hellospringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.sdacademy.hellospringboot.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u where u.firstName = :firstName")
    List<User> findByFirstName(@Param("firstName") String firstName);

    List<User> findAllByFirstNameIgnoringCase(String firstName);

    List<User> findAllByFirstNameNotOrLastNameNot(String firstName, String lastName);

    List<User> findAllByFirstNameIn(List<String> firstNames);

    Optional<User> findFirstByFirstName(String firstName);
}
