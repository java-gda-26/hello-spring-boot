package pl.sdacademy.hellospringboot.service;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.sdacademy.hellospringboot.entity.User;

@Service
@SessionScope
public class CurrentUserService {

    private User user;

    public void saveCurrentUser(String firstName, String lastName) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        this.user = user;
    }

    public void saveCurrentUser(User user) {
        this.user = user;
    }

    public User fetchCurrentUser() {
        if (user == null) {
            user = new User();
            user.setFirstName("Thymeleaf");
        }

        return user;
    }
}
