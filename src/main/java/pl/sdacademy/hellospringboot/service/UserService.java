package pl.sdacademy.hellospringboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.hellospringboot.entity.User;
import pl.sdacademy.hellospringboot.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public Collection<User> findAll(String firstName) {
        if (firstName == null) {
            return userRepository.findAll();
        }

        return userRepository.findAllByFirstNameIgnoringCase(firstName);
    }

    public User findUser(Long id) {
        return userRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
