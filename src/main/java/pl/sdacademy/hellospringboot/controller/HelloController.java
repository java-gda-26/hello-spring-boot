package pl.sdacademy.hellospringboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.hellospringboot.entity.User;
import pl.sdacademy.hellospringboot.service.CurrentUserService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/hello")
public class HelloController {

    private final CurrentUserService currentUserService;

    @GetMapping
    public String hello(
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String lastName,
            Model model
    ) {
        if (firstName != null && lastName != null) {
            currentUserService.saveCurrentUser(firstName, lastName);
        }

        String firstNameAttr = currentUserService.fetchCurrentUser().getFirstName();
        model.addAttribute("firstName", firstNameAttr);
        return "hello_template";
    }

    @PostMapping
    public String handleUserForm(@ModelAttribute("userForm") User user) {
        currentUserService.saveCurrentUser(user);
        return "redirect:hello";
    }

    @ModelAttribute("userForm")
    public User produceUser() {
        return new User();
    }
}