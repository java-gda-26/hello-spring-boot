package pl.sdacademy.hellospringboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.hellospringboot.entity.User;
import pl.sdacademy.hellospringboot.service.UserService;

import java.util.Collection;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserRestController {

    private final UserService userService;

    @GetMapping
    public Collection<User> findAllUsers(
            @RequestParam(required = false, name = "sort") String sortOrder,
            @RequestParam(required = false) String firstName
    ) {
        return userService.findAll(firstName);
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.findUser(id);
    }

    @PostMapping
    public User saveUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.delete(id);
    }
}
